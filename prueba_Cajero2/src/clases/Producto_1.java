/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Renzo
 */
@Entity
@Table(name = "producto", catalog = "supermercado", schema = "")
@NamedQueries({
    @NamedQuery(name = "Producto_1.findAll", query = "SELECT p FROM Producto_1 p"),
    @NamedQuery(name = "Producto_1.findByCodBarras", query = "SELECT p FROM Producto_1 p WHERE p.codBarras = :codBarras"),
    @NamedQuery(name = "Producto_1.findByNombre", query = "SELECT p FROM Producto_1 p WHERE p.nombre = :nombre"),
    @NamedQuery(name = "Producto_1.findByPvp", query = "SELECT p FROM Producto_1 p WHERE p.pvp = :pvp"),
    @NamedQuery(name = "Producto_1.findByDescripcion", query = "SELECT p FROM Producto_1 p WHERE p.descripcion = :descripcion")})
public class Producto_1 implements Serializable {
    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "COD_BARRAS")
    private Long codBarras;
    @Basic(optional = false)
    @Column(name = "NOMBRE")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "PVP")
    private float pvp;
    @Column(name = "DESCRIPCION")
    private String descripcion;

    public Producto_1() {
    }

    public Producto_1(Long codBarras) {
        this.codBarras = codBarras;
    }

    public Producto_1(Long codBarras, String nombre, float pvp) {
        this.codBarras = codBarras;
        this.nombre = nombre;
        this.pvp = pvp;
    }

    public Long getCodBarras() {
        return codBarras;
    }

    public void setCodBarras(Long codBarras) {
        Long oldCodBarras = this.codBarras;
        this.codBarras = codBarras;
        changeSupport.firePropertyChange("codBarras", oldCodBarras, codBarras);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        String oldNombre = this.nombre;
        this.nombre = nombre;
        changeSupport.firePropertyChange("nombre", oldNombre, nombre);
    }

    public float getPvp() {
        return pvp;
    }

    public void setPvp(float pvp) {
        float oldPvp = this.pvp;
        this.pvp = pvp;
        changeSupport.firePropertyChange("pvp", oldPvp, pvp);
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        String oldDescripcion = this.descripcion;
        this.descripcion = descripcion;
        changeSupport.firePropertyChange("descripcion", oldDescripcion, descripcion);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codBarras != null ? codBarras.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Producto_1)) {
            return false;
        }
        Producto_1 other = (Producto_1) object;
        if ((this.codBarras == null && other.codBarras != null) || (this.codBarras != null && !this.codBarras.equals(other.codBarras))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "clases.Producto_1[ codBarras=" + codBarras + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
