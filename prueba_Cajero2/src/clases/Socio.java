package clases;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.GregorianCalendar;

/**
 *
 * @author Renzo
 */
public class Socio extends Persona implements Comparable<Persona>{

    //Atributos
    private int numSocio;
    private static int puntos;
        
    //Constructor
    public Socio(String nif, String nom, int num) {
        super(nif, nom);
        numSocio = num;        
    }

    
    //Métodos

    public int getNumSocio() {
        return numSocio;
    }

    public void setNumSocio(int numSocio) {
        this.numSocio = numSocio;
    }

    public static int getPuntos() {
        return puntos;
    }

    public static void setPuntos(int puntos) {
        Socio.puntos = puntos;
    }

    @Override
    public String toString() {
        return "Núero de oocio: " + numSocio;
    }      
    
    @Override
    public int compareTo(Persona o) {        
        return o.getNIF().compareTo(o.getNIF());        
    }                
    
}
