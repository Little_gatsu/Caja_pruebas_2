package clases;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Hashtable;

/**
 *
 * @author Renzo
 */
public class Compra{
    
    //Atributos    
    private Hashtable<Producto, Integer> unidadesXproducto;
    private float importe;
    private float descuento;
    private boolean esSocio;

    //Constructor
    public Compra(Hashtable<Producto, Integer> unidadesXproducto, float importe){        
        this.unidadesXproducto = unidadesXproducto;
        this.importe = importe;
    }
    
    
    
    //Métodos

    public Hashtable<Producto, Integer> getUnidadesXproducto() {
        return unidadesXproducto;
    }

    public void setUnidadesXproducto(Hashtable<Producto, Integer> unidadesXproducto) {
        this.unidadesXproducto = unidadesXproducto;
    }

    public float getImporte() {
        return importe;
    }

    public void setImporte(float importe) {
        this.importe = importe;
    }

    public float getDescuento() {
        return descuento;
    }

    public void setDescuento(float descuento) {
        this.descuento = descuento;
    }

    public boolean isEsSocio() {
        return esSocio;
    }

    public void setEsSocio(boolean esSocio) {
        this.esSocio = esSocio;
    }

    @Override
    public String toString() {
        return "Compra{" + "unidadesXproducto=" + unidadesXproducto + ", importe=" + importe + ", descuento=" + descuento + ", esSocio=" + esSocio + '}';
    }
                      
}
