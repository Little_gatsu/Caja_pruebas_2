package clases;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Renzo
 */
public class Persona {
    
    //Atributos
    private String NIF;
    private String nombre;    
    
    //Constructor
    public Persona(String nif, String nom){
        NIF = nif;
        nombre = nom;        
    }
    
    //Métodos

    public String getNIF() {
        return NIF;
    }

    public void setNIF(String NIF) {
        this.NIF = NIF;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }   

    @Override
    public String toString() {
        return  "NIF=" + NIF + ", nombre=" + nombre;
    }                    
    
}
