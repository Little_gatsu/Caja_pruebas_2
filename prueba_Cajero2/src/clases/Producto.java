package clases;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.util.GregorianCalendar;

/**
 *
 * @author Renzo
 */
public class Producto{
    
    //Atributos    
    private String codigo;
    private String nombre;
    private float precio;    
    private boolean se_come;
    
    //Constructor
    public Producto(String codigo, String nombre, float precio){        
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }
    

    public boolean isSe_come() {
        return se_come;
    }

    public void setSe_come(boolean se_come) {
        this.se_come = se_come;
    }

    @Override
    public String toString() {
        return  nombre + " " + precio ;
    }                            
    
}
