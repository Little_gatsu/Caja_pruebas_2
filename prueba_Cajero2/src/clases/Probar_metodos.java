package clases;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Renzo
 */
//Esta clase la utilizo para probar el funcionamiento de métodos en un ArrayList de Objetos para
//poder manipularlos
public class Probar_metodos {        
            
    public static void main(String[] args) {
        //Creamos el array List de Productos:
        ArrayList<Producto> productos = new ArrayList<Producto>();                
        
        //Concetamos con la base de datos de supermercado para acceder a los datos,
        //P.D: Necesitamos tener creada la clase Conexion anteriormente.
        Connection conexion = null;
        conexion  = Conexion.mySQL("supermercado", "root", "");
    
        if (conexion != null) { 
            System.out.println("Conexion realizada con éxito");
            //Paso 2: Ejecutar una consulta             
            try {
                ResultSet rs = null;
                Statement sentencia = conexion.createStatement();
                //Hacemos la consulta que nos permita sacar los datos necesarios para 
                //Crear cada uno de los Objetos Producto
                String sql = "SELECT COD_BARRAS, NOMBRE, PVP FROM producto;";
                rs = sentencia.executeQuery(sql);
                //Paso 3: Extrayendo los datos del resultset                 
                while (rs.next()) { //Vamos extrayendo los valores de cada fila.
                    //Recuperar datos por el nombre de la columna                     
                    String cod_barras = rs.getString("COD_BARRAS");
                    String nombre = rs.getString("NOMBRE");
                    String pvp = rs.getString("PVP");                    
                    //Una vez tengamos los valores contenidos en variables, podemos ir 
                    //creando los Objetos pasandoles los parametros
                    productos.add(new Producto(cod_barras, nombre, Float.parseFloat(pvp)));                    
                } //El bucle se repetirá hasta que no haya más filas que leer de la tabla

            } catch (SQLException ex) {
                System.out.println("Error");
            }
        }
        
        
        System.out.println("Tamaño del array: "+productos.size());
        
        
        for (int i = 0; i< productos.size(); i++) {            
        System.out.println("Codigo de barras: "+productos.get(i).getCodigo()+" Nombre: "+productos.get(i).getNombre()+" Precio: "+productos.get(i).getPrecio());            
        }
                        
    }                
    
}
